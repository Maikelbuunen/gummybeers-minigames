﻿using System.Collections;
using System;
using UnityEngine;

using ButtonSide = GummyBeers.SmashButton.ButtonSide;

namespace GummyBeers
{
    public class BombSpawner : MonoBehaviour
    {
        [SerializeField] private Transform _bombHolder;
        [SerializeField] private GameObject _bombPrefab;
        [SerializeField] private float _waitTime;

        private bool _gameEnded = false;

        private Bomb _currentBomb;
        private RowManager _rowManager;

        public event Action<Bomb> OnNewBombSpawned;

        private void Start()
        {
            _rowManager = GetComponentInParent<RowManager>();

            FindObjectOfType<GameTimer>().OnGameEnded += OnGameEnded;

            SpawnNewBomb();
        }

        private void OnBombExplode(ButtonSide buttonSide)
        {
            _rowManager.ResetPlank();
            SpawnNewBomb();
        }
        private void SpawnNewBomb()
        {
            if (_gameEnded)
                return;

            StartCoroutine(WaitBeforeSpawn());

            GameObject bombObj = GameObject.Instantiate(_bombPrefab, _bombHolder);

            bombObj.transform.localPosition = new Vector3(0, 0.1f, 0);
            _currentBomb = bombObj.GetComponent<Bomb>();
            _currentBomb.OnExplode += OnBombExplode;

            if (OnNewBombSpawned != null)
                OnNewBombSpawned(_currentBomb);
        }

        private IEnumerator WaitBeforeSpawn()
        {
            yield return new WaitForSeconds(_waitTime);
        }

        private void OnGameEnded()
        {
            _gameEnded = true;
        }
    }
}