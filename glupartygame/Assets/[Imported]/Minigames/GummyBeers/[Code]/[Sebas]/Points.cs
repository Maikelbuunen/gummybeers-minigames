﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Points : MonoBehaviour
{

    private int m_ScoreTeam1 = 0, m_ScoreTeam2 = 0;

    void Update()
    {

    }

    public void PointTeam1(int points)
    {
        points += m_ScoreTeam1;
    }
    public void PointTeam2(int points)
    {
        points += m_ScoreTeam2;
    }
}
