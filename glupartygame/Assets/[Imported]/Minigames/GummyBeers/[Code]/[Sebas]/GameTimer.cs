﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimer : MonoBehaviour
{
    [SerializeField] private int _gameTime = 300;

    private float _currentGameTime;

    private bool _10SecondsLeft = false;
    private bool _30SecondsLeft = false;
    private bool _1MinuteLeft = false;
    private bool _gameEnded = false;
    
    public event Action<int> TimeMilestone;
    public event Action OnGameEnded;

    private void Start()
    {
        _currentGameTime = _gameTime;
    }
    private void Update()
    {
        _currentGameTime -= 1f * Time.deltaTime;

        if (!_10SecondsLeft && _currentGameTime <= 10f)
        {
            _10SecondsLeft = true;

            if (TimeMilestone != null)
                TimeMilestone.Invoke(10);
        }
        if (!_1MinuteLeft && _currentGameTime <= 60f)
        {
            _1MinuteLeft = true;

            if (TimeMilestone != null)
                TimeMilestone.Invoke(60);
        }
        if (!_30SecondsLeft && _currentGameTime <= 30f)
        {
            _30SecondsLeft = true;

            if (TimeMilestone != null)
                TimeMilestone.Invoke(30);
        }

        if (!_gameEnded && _currentGameTime <= 0f)
            GameEnded();

    }

    public string GetTimeString()
    {
        int minLeft = (int)(_currentGameTime / 60f);
        int secLeft = (int)(_currentGameTime - (minLeft * 60f));

        if (secLeft >= 10)
            return "0" + minLeft + ":" + secLeft;
        else
            return "0" + minLeft + ":" + "0" + secLeft;
    }

    private void GameEnded()
    {
        if (OnGameEnded != null)
            OnGameEnded();
    }
}
