﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoryOnTime : MonoBehaviour
{
    [SerializeField] private float _seconds;

    private void Start()
    {
        StartCoroutine(Wait());
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(_seconds);
        Destroy(this.gameObject);
    }
}
