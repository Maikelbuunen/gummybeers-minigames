﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamManager : MonoBehaviour
{
    [SerializeField] private Texture[] _playerTextures;
    [SerializeField] private Material _sourceMaterial;

    public Texture GetPlayerTexture(int PlayerIndex)
    {
        if (PlayerIndex > -1 && PlayerIndex < 4)
        {
            return _playerTextures[PlayerIndex];
        }

        return null;
    }
    public Material GetSourceMaterial()
    {
        return _sourceMaterial;
    }
}
