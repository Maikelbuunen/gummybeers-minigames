﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GummyBeers
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private Text _text;

        private GameTimer _timer;
        private ScoreManager _score;

        void Start()
        {
            _timer = GetComponent<GameTimer>();
            _score = GetComponent<ScoreManager>();
        }
        void Update()
        {
            UpdateUI();
        }

        private void UpdateUI()
        {
            _text.text = _score.LeftScore + " -| " + _timer.GetTimeString() + " |- " + _score.RightScore; 
        }
    }
}