﻿using System;
using System.Collections.Generic;
using UnityEngine;

using ButtonSide = GummyBeers.SmashButton.ButtonSide;

namespace GummyBeers
{
    public class Bomb : MonoBehaviour
    {
        [SerializeField] private float _moveSpeed;
        [SerializeField] private float _lerpSpeed;

        private Vector3 _currentDirection;
        private Vector3 _targetDirection;

        private bool _gameEnded = false;

        public event Action<ButtonSide> OnExplode;

        private void Start()
        {
            GetComponentInParent<RowManager>().OnPlankSmashed += ChangeDirection;
            FindObjectOfType<GameTimer>().OnGameEnded += OnGameEnded;
        }
        private void Update()
        {
            if (_gameEnded)
                return;

            _currentDirection = Vector3.Lerp(_currentDirection, _targetDirection, _lerpSpeed);
            _currentDirection = transform.TransformDirection(_currentDirection);

            transform.position += _currentDirection * _moveSpeed * Time.deltaTime;

            if (transform.localPosition.z <= -2.87f || transform.localPosition.z >= 2.87f)
                Explode();
        }

        private void ChangeDirection(ButtonSide buttonSide)
        {
            if (buttonSide == ButtonSide.Left)
            {
                _targetDirection = Vector3.forward;
            }
            else
            {
                _targetDirection = Vector3.back;
            }
        }
        private void Explode()
        {
            ButtonSide side;

            if (transform.localPosition.z > 0)
                side = ButtonSide.Left;
            else
                side = ButtonSide.Right;

            if (OnExplode != null)
                OnExplode(side);

            GameObject.Destroy(this.gameObject);
        }

        private void OnGameEnded()
        {
            _gameEnded = true;
        }
    }
}
