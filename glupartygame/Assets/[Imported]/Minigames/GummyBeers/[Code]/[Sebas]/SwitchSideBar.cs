﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchSideBar : MonoBehaviour
{
    private float _Abutton, _Xbutton;

    [SerializeField] private Transform[] m_Holders = new Transform[2];

    [SerializeField] private Transform m_StartposTeam1, m_StartposTeam2;

    private Points m_PointSystem;

    void Update()
    {
        _Abutton = Input.GetAxis("Submit");// Button A op controller
        _Xbutton = Input.GetAxis("Fire3");// Button X op controller
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Team1"))
        {
            if (_Abutton > 0)
            {
                _Xbutton = 0;
                m_Holders[1].position = Vector3.Lerp(m_Holders[1].position, new Vector3(m_StartposTeam2.position.x, m_StartposTeam2.position.y - 0.6f, m_StartposTeam2.position.z), 4f * Time.deltaTime);
                m_Holders[0].position = Vector3.Lerp(m_Holders[0].position, new Vector3(m_StartposTeam1.position.x, m_StartposTeam1.position.y, m_StartposTeam1.position.z), 4f * Time.deltaTime);
            }
        }
        if (other.CompareTag("Team2"))
        {
            if (_Xbutton > 0)
            {
                _Abutton = 0;
                m_Holders[0].position = Vector3.Lerp(m_Holders[0].position, new Vector3(m_StartposTeam1.position.x, m_StartposTeam1.position.y - 0.6f, m_StartposTeam1.position.z), 4f * Time.deltaTime);
                m_Holders[1].position = Vector3.Lerp(m_Holders[1].position, new Vector3(m_StartposTeam2.position.x, m_StartposTeam2.position.y, m_StartposTeam2.position.z), 4f * Time.deltaTime);
            }
        }
    }
}
