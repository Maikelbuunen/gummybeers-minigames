﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GummyBeers
{
    public class Team : MonoBehaviour
    {
        private Driver _driver;
        private Smasher _smasher;
        private int _teamId;

        private void Awake()
        {
            //Get the team id from the transform.name
            int result;
            if(int.TryParse(name[name.Length - 1].ToString(), out result))
                _teamId = result;
            else
                Debug.LogError("Set the name of this object to: 'Team 1' or 'Team 2'");

            _driver = GetComponent<Driver>();
            _smasher = GetComponent<Smasher>();

            if(_teamId == 1)
            {
                _driver.SetPlayerIndex(_teamId - 1);
                _smasher.SetPlayerIndex(_teamId);
            }
            else
            {
                _driver.SetPlayerIndex(_teamId );
                _smasher.SetPlayerIndex(_teamId + 1);
            }
        }

        public int TeamID
        {
            get { return _teamId; }
        }
    }
}
