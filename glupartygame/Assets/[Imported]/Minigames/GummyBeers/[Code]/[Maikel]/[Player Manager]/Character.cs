﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private MeshRenderer _renederer;

    private MeshRenderer[] _childRenderers;
    private Material[] _oldMaterials;
    private Material _newMaterial;

    private bool _gameEnded = false;

    protected int _playerIndex;

    public void SetPlayerIndex(int index)
    {
        _playerIndex = index;
    }

    protected void LateStart()
    {
        FindObjectOfType<GameTimer>().OnGameEnded += OnGameEnded;

        _oldMaterials = GetOldMaterials();
        CreateDynamicMaterial(FindObjectOfType<TeamManager>().GetPlayerTexture(_playerIndex));
    }

    private void CreateDynamicMaterial(Texture texture)
    {
        //Create new materials
        _newMaterial = new Material(FindObjectOfType<TeamManager>().GetSourceMaterial());
        _newMaterial.SetTexture("_MainTex", texture);

        //Apply new material
        for (int i = 0; i < _childRenderers.Length; i++)
        {
            _childRenderers[i].material = _newMaterial;
        }
    }

    protected void StartUpdate()
    {
        if (_gameEnded)
            return;
    }

    private Material[] GetOldMaterials()
    {
        List<Material> materials = new List<Material>();

        //get all the materials from the renderers
        _childRenderers = _renederer.transform.GetComponentsInChildren<MeshRenderer>();
        for(int i = 0; i < _childRenderers.Length; i++)
        {
            materials.AddRange(_childRenderers[i].materials);
        }

        //filter out all the non-character materials
        for(int i = 0; i < materials.Count; i++)
        {
            if (!materials[i].name.Contains("Char_Basic"))
            {
                materials.Remove(materials[i]);
            }
        }

        return materials.ToArray();
    }

    private void OnGameEnded()
    {

    }
}
