﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XBOXParty;


namespace GummyBeers
{
    public class Smasher : Character
    {
        [SerializeField] private Vector3 _raycastStart;
        [SerializeField] private Vector3 _raycastDirection;
        [SerializeField] private float _hitCooldown;
        private float _currentCooldown;

        private void Start()
        {
            //_playerIndex = GetComponentInParent<Team>().TeamID + 1;

            //binding buttons
            InputManager.Instance.BindButton("BomberMan_Swing_" + (_playerIndex) , _playerIndex, ControllerButtonCode.A, ButtonState.OnPress);

            LateStart();
        }
        private void Update()
        {
            base.StartUpdate();

            _hitCooldown -= 1f * Time.deltaTime; ;

            if (_hitCooldown > 0)
                return;

            //swing on input
            if (InputManager.Instance.GetButton("BomberMan_Swing_" + (_playerIndex)))
            {
                Swing();
            }
        }

        private void Swing()
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position + _raycastStart, transform.TransformDirection(_raycastDirection), out hit))
            {
                SmashButton hitButton = hit.transform.GetComponent<SmashButton>();
                if(hitButton != null)
                {
                    hitButton.HitButton();
                }
            }

            _currentCooldown = _hitCooldown;
        }
    }
}
