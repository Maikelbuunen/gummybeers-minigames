﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XBOXParty;


namespace GummyBeers
{
    public class Driver : Character
    {
        [SerializeField] private float _moveAmount;
        [SerializeField] private float _smoothTime = 0.3f;
        [SerializeField] private int _position;

        private Vector3 _targetPosition;
        private Vector3 _velocity = Vector3.zero;

        private enum MoveDirection { Up, Down }

        private void Start()
        {
            //int teamID = GetComponent<Team>().TeamID;

            //binding the buttons
            InputManager.Instance.BindButton("BomberMan_Move_Up_" + _playerIndex, _playerIndex, ControllerButtonCode.Up, ButtonState.OnPress);
            InputManager.Instance.BindButton("BomberMan_Move_Down_" + _playerIndex, _playerIndex, ControllerButtonCode.Down, ButtonState.OnPress);

            _targetPosition = transform.position;

            LateStart();
        }

        private void Update()
        {
            base.StartUpdate();

            //updating the targetposition through input
            if (InputManager.Instance.GetButton("BomberMan_Move_Down_" + _playerIndex))
            {
                if (CanMove(MoveDirection.Down))
                {
                    _targetPosition += new Vector3(-_moveAmount, 0, 0);
                    _position++;
                }
            }
            else if (InputManager.Instance.GetButton("BomberMan_Move_Up_" + _playerIndex))
            {
                if (CanMove(MoveDirection.Up))
                {
                    _targetPosition += new Vector3(_moveAmount, 0, 0);
                    _position--;
                }
            }

            transform.position = Vector3.SmoothDamp(transform.position, _targetPosition, ref _velocity, _smoothTime);
        }

        private bool CanMove(MoveDirection direction)
        {
            if(direction == MoveDirection.Up)
            {
                return (_position > 0);
            }
            else
            {
                return (_position < 5);
            }
        }
    }
}
