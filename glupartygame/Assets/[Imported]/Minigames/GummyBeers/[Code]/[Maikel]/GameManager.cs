﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XBOXParty;

namespace GummyBeers
{
    public class GameManager : MonoBehaviour
    {
        private List<int> _positions;
        [SerializeField] private GameObject _teamPrefab;

        private void Start()
        {
            _positions = new List<int>();

            GlobalGameManager.Instance.SetPlayerCount(2);
            int playerCount = GlobalGameManager.Instance.PlayerCount;

            for (int i = 0; i < playerCount; ++i)
            {
                _positions.Add(i);

                if(i % 2 == 0)
                {
                    SpawnTeam(i);
                }
                //SpawnPlayer(i);
            }

            //Bind button
            InputManager.Instance.BindButton("BomberMan_Submit", 0, ControllerButtonCode.A, ButtonState.OnPress);
        }
        public void Update()
        {
            if (InputManager.Instance.GetButton("BomberMan_Submit"))
            {
                //Submit();
            }
        }

        private void SpawnTeam(int index)
        {
            GameObject team = GameObject.Instantiate(_teamPrefab);
            team.name = "Team " + index;
        }
        public void Submit()
        {
            GlobalGameManager.Instance.SubmitGameResults(_positions);
        }
    }
}
