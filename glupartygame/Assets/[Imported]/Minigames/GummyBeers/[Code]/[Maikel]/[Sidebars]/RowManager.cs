﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ButtonSide = GummyBeers.SmashButton.ButtonSide;
using ButtonState = GummyBeers.SmashButton.ButtonState;

namespace GummyBeers
{
    public class RowManager : MonoBehaviour
    {
        [SerializeField] private Transform _plankTransform;
        [SerializeField] private float _plankSmoothTime;

        [Header("Button Textures")]
        [SerializeField] private Material _baseMaterial;
        [SerializeField] private Texture _positiveTexture;
        [SerializeField] private Texture _negativeTexture;
        [SerializeField] private Texture _neutralTexture;

        private SmashButton[] _buttons;

        //fix bomb physics
        private Vector3 _targetRotation = Vector3.zero;
        public event Action<ButtonSide> OnPlankSmashed;

        private void Start()
        {
            _buttons = GetComponentsInChildren<SmashButton>();

            _buttons[0].OnButtonSmashed += HandleButtonSmash;
            _buttons[1].OnButtonSmashed += HandleButtonSmash;

        }
        private void Update()
        {
            //Lerp the plank rotation
            Quaternion currentRotation = _plankTransform.transform.rotation;
            _plankTransform.transform.rotation = Quaternion.Lerp(currentRotation, Quaternion.Euler(_targetRotation), _plankSmoothTime * Time.deltaTime);
        }

        public void ResetPlank()
        {
            _targetRotation = Vector3.zero;
            ResetButtonTextures();
        }

        private void HandleButtonSmash(ButtonSide buttonSide)
        {
            ChangeRotation(buttonSide);
            UpdateButtonTextures(buttonSide);
        }
        private void ChangeRotation(ButtonSide buttonSide)
        {
            switch (buttonSide)
            {
                case ButtonSide.Left:

                    _targetRotation = new Vector3(-2.5f, 0, 0);
                    OnPlankSmashed(ButtonSide.Right);
                    break;

                case ButtonSide.Right:

                    _targetRotation = new Vector3(2.5f, 0, 0);
                    OnPlankSmashed(ButtonSide.Left);
                    break;
            }
        }
        private void UpdateButtonTextures(ButtonSide buttonSide)
        {
            if(_buttons[0].GetButtonSide() == buttonSide)
            {
                _buttons[0].ChangeTexture(_positiveTexture);
                _buttons[1].ChangeTexture(_negativeTexture);
            }
            else
            {
                _buttons[0].ChangeTexture(_negativeTexture);
                _buttons[1].ChangeTexture(_positiveTexture);
            }
        }
        private void ResetButtonTextures()
        {
            _buttons[0].ChangeTexture(_neutralTexture);
            _buttons[1].ChangeTexture(_neutralTexture);
        }

        public Material ButtonBaseMaterial
        {
            get { return _baseMaterial; }
        }
        public Texture GetButtonMaterial(ButtonState buttonState)
        {
            switch (buttonState)
            {
                case ButtonState.Negative:
                    return _negativeTexture;
                case ButtonState.Positive:
                    return _positiveTexture;
                case ButtonState.Neutral:
                    return _neutralTexture;
            }

            return null;
        }
    }
}