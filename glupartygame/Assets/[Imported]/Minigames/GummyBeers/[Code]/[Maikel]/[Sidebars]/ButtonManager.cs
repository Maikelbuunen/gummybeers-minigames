﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GummyBeers
{
    public class ButtonManager : MonoBehaviour
    {
        [Serializable]
        private struct RowButtons
        {
            public SmashButton leftButton;
            public SmashButton rightButton;
        }

        [SerializeField] private RowButtons _row1Buttons;
        [SerializeField] private RowButtons _row2Buttons;
        [SerializeField] private RowButtons _row3Buttons;

        private void Update()
        {
        }
    }
}
