﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Object = UnityEngine.Object;

namespace GummyBeers
{
    public class SmashButton : MonoBehaviour
    {
        public enum ButtonSide { Left, Right }
        public enum ButtonState { Negative, Positive, Neutral}

        [SerializeField] private ButtonSide _buttonSide;
        private Material _dynamicMaterial;

        public event Action<ButtonSide> OnButtonSmashed;

        private ParticleManager _particleManager;

        private void Start()
        {
            //Create dynamic material
            _dynamicMaterial = new Material(GetComponentInParent<RowManager>().ButtonBaseMaterial);

            //Apply dynamic material
            GetComponentInChildren<MeshRenderer>().material = _dynamicMaterial;

            //Set button texture to Nuteral
            ChangeTexture(GetComponentInParent<RowManager>().GetButtonMaterial(ButtonState.Neutral));

            _particleManager = FindObjectOfType<ParticleManager>();
        }

        public void HitButton()
        {
            GameObject particle = GameObject.Instantiate(_particleManager.Sparks, (transform.position + new Vector3(-0.019f, 0.3914615f, -0.195f)), Quaternion.identity);

            if(OnButtonSmashed != null)
            {
                OnButtonSmashed(_buttonSide);
            }
        }
        public void ChangeTexture(Texture texture)
        {
            _dynamicMaterial.SetTexture("_MainTex", texture);
        }

        public ButtonSide GetButtonSide()
        {
            return _buttonSide;
        }
    }
}
