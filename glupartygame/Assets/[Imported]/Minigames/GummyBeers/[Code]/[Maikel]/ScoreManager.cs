﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ButtonSide = GummyBeers.SmashButton.ButtonSide;

namespace GummyBeers
{
    public class ScoreManager : MonoBehaviour
    {
        private BombSpawner[] _bombSpawner;

        public int LeftScore { get; internal set; }
        public int RightScore { get; internal set; }

        private void Awake()
        {
            _bombSpawner = FindObjectsOfType<BombSpawner>();
            for(int i = 0; i < _bombSpawner.Length; i++)
            {
                _bombSpawner[i].OnNewBombSpawned += OnNewBombSpawned;
            }
        }

        private void OnNewBombSpawned(Bomb bomb)
        {
            bomb.OnExplode += OnBombExplode;
        }
        private void OnBombExplode(ButtonSide buttonSide)
        {
            if (buttonSide == ButtonSide.Left)
                RightScore++;
            else
                LeftScore++;
        }
    }
}