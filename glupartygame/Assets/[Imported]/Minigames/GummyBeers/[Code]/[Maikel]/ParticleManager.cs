﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GummyBeers
{
    public class ParticleManager : MonoBehaviour
    {
        [SerializeField] private GameObject _sparks;

        public GameObject Sparks { get { return _sparks; } }
    }
}